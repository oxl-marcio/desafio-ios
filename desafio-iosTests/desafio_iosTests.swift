//
//  desafio_iosTests.swift
//  desafio-iosTests
//
//  Created by Marcio Garcia on 1/20/17.
//  Copyright © 2017 Marcio Garcia. All rights reserved.
//

import XCTest
@testable import desafio_ios
import ObjectMapper

class desafio_iosTests: XCTestCase {
    
    var repoViewController: RepositoriesViewController!
    
    let helper = Helper()
    
    var JSONRepo: [String: AnyObject]?
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        let bundle = NSBundle.mainBundle()
        self.repoViewController = UIStoryboard(name: "Main", bundle: bundle).instantiateViewControllerWithIdentifier("RepositoriesVC") as! RepositoriesViewController
        
        self.JSONRepo = self.helper.loadJSON("repositories")
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // Unit Test
    func testThatItGetsJsonResult() {
        
        // Given
        let searchCriteria = "language:Java"
        let sortType = SortType.Stars
        let page = 1
        
        // When
        GitHubService.getRepositories(searchCriteria, sort: sortType, page: page) { (result) in
            
            let trimmedResult = result!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            
            let firstChar = trimmedResult.characters.first!
            let lastChar = trimmedResult.characters.last!
            
        // Then
            XCTAssertEqual(firstChar, "{", "@error: first character must be '{'.")
            XCTAssertEqual(lastChar, "}", "@error: first character must be '}'.")
        }
    }
    
    
    // Functional Test
    func testThatItLoadsTheCorrectNumberOfRepositoriesToTheTableView() {
        // Given
        let result = RequestResult(JSON: self.JSONRepo!)
        
        // When
        for item in result!.items! {
            self.repoViewController.itemList.append(item)
        }
        
        let _ = self.repoViewController.view
        
        // Then
        XCTAssertEqual(self.repoViewController.repoTableView.numberOfRowsInSection(0), 30, "@error: number of repositories in the table view must be 30.")
    }
    
}

class Helper {
    
    func loadJSON(fileName: String) -> [String: AnyObject]? {
        
        guard let pathString = NSBundle(forClass: self.dynamicType).pathForResource(fileName, ofType: "json") else {
            fatalError(fileName + " not found")
        }
        
        guard let jsonString = try? NSString(contentsOfFile: pathString, encoding: NSUTF8StringEncoding) else {
            fatalError("Unable to convert " + fileName + ".json to String")
        }
        
        guard let jsonData = jsonString.dataUsingEncoding(NSUTF8StringEncoding) else {
            fatalError("Unable to convert " + fileName + ".json to NSData")
        }
        
        guard let jsonDictionary = try? NSJSONSerialization.JSONObjectWithData(jsonData, options: []) as? [String:AnyObject] else {
            fatalError("Unable to convert " + fileName + " to JSON dictionary")
        }
        
        return jsonDictionary
    }
}

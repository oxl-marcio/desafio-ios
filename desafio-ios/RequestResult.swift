//
//  RequestResult.swift
//  desafio-ios
//
//  Created by Marcio Garcia on 1/21/17.
//  Copyright © 2017 Marcio Garcia. All rights reserved.
//

import Foundation
import ObjectMapper

class RequestResult: Mappable {
    
    var totalCount: Int?
    var incompleteResults: String?
    var items: [Repository]?

    required init?(_ map: Map){}

    func mapping(map: Map) {
        totalCount <- map["total_count"]
        incompleteResults <- map["incomplete_results"]
        items <- map["items"]
    }

}
//
//  PullRequest.swift
//  desafio-ios
//
//  Created by Marcio Garcia on 1/22/17.
//  Copyright © 2017 Marcio Garcia. All rights reserved.
//

import Foundation
import ObjectMapper

class PullRequest: Mappable {

    var id: Int?
    var title: String?
    var body: String?
    var htmlUrl: String?
    var createdAt: String?
    var state: String?

    var user: Owner?

    required init?(_ map: Map){}

    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        body <- map["body"]
        htmlUrl <- map["html_url"]
        createdAt <- map["created_at"]
        state <- map["state"]
        user <- map["user"]
    }

}
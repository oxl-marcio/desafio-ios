//
//  Repository.swift
//  desafio-ios
//
//  Created by Marcio Garcia on 1/21/17.
//  Copyright © 2017 Marcio Garcia. All rights reserved.
//

import Foundation
import ObjectMapper

class Repository: Mappable {
    
    var id: Int?
    var name: String?
    var fullName: String?
    var description: String?
    var starsCount: Int?
    var forksCount: Int?
    var pullsUrl: String?
    var owner: Owner?

    var pullsUrlFinal:String? {
        return pullsUrl?.stringByReplacingOccurrencesOfString("{/number}", withString: "") ?? ""
    }
    
    required init?(_ map: Map){}

    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        description <- map["description"]
        fullName <- map["full_name"]
        starsCount <- map["stargazers_count"]
        forksCount <- map["forks_count"]
        pullsUrl <- map["pulls_url"]
        owner <- map["owner"]
    }

}
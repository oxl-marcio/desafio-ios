//
//  RepositoryTableViewCell.swift
//  desafio-ios
//
//  Created by Marcio Garcia on 1/21/17.
//  Copyright © 2017 Marcio Garcia. All rights reserved.
//

import UIKit

class RepositoryTableViewCell: UITableViewCell {

    @IBOutlet weak var ownerImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var ownerImageActivityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var ownerLoginLabel: UILabel!
    @IBOutlet weak var ownerNameLabel: UILabel!
    @IBOutlet weak var forksCountLabel: UILabel!
    @IBOutlet weak var starsCountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.containerView.layer.cornerRadius = 4.0
        self.containerView.layer.borderWidth = 1
        self.containerView.layer.borderColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.1).CGColor
    }

    override func drawRect(rect: CGRect) {
        self.ownerImageView.layer.cornerRadius = 5.0
        self.ownerImageView.clipsToBounds = true
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(repository: Repository) {
        
        if let title = repository.fullName {
            self.titleLabel.text = title
        }
        else {
            self.titleLabel.text = "No Title"
        }
        
        if let description = repository.description {
            self.descriptionLabel.text = description
        }
        
        if let forks = repository.forksCount {
            self.forksCountLabel.text = "\(forks)"
        }
        
        if let stars = repository.starsCount {
            self.starsCountLabel.text = "\(stars)"
        }
        
        if  let owner = repository.owner {
            

            self.ownerLoginLabel.text = owner.login
            
            if owner.name?.characters.count > 0 {
                self.ownerNameLabel.text = owner.name
            }
            else {
                if let url = owner.url {
                    GitHubService.getUser(url, completion: { (result) in
                        if let result = result {
                            let user = Owner(JSONString: result)
                            owner.name = user?.name
                            self.ownerNameLabel.text = owner.name
                        }
                    })
                }
            }
            
            owner.cancelImageRequest()
            self.ownerImageView.image = nil
            self.ownerImageActivityIndicatorView.startAnimating()
            
            owner.getProfileImage({ (image) in
                if let image = image {
                    self.ownerImageView.image = image
                    self.ownerImageActivityIndicatorView.stopAnimating()
                }
            })
            
        }
    }

}

//
//  Constants.swift
//  desafio-ios
//
//  Created by Marcio Garcia on 1/21/17.
//  Copyright © 2017 Marcio Garcia. All rights reserved.
//

import UIKit


// Storyboard
let STORYBOARD_MAIN = "Main"
let XIB_REPO_TABLE_CELL = "RepositoryTableViewCell"
let XIB_PULL_REQUEST_TABLE_CELL = "PullRequestTableViewCell"
let VC_LANGUAGE = "LanguageVC"

// ID
let ID_REPO_TABLE_CELL = "RepositoryCell"
let ID_PULL_REQUEST_TABLE_CELL = "PullRequestCell"

// Segues
let SEGUE_PULL_REQUEST = "PullRequest"
let SEGUE_SELECT_LANGUAGE = "SelectLanguage"

// Colors
let COLOR_RED: UIColor = UIColor(red: 236/255, green: 85/255, blue: 101/255, alpha: 1)
let COLOR_NAVIGATION_BAR_TINT: UIColor = COLOR_RED
let COLOR_NAVIGATION_BAR_TITLE: UIColor = UIColor.whiteColor()

// Files
let FILE_LANGUAGES = "languages"

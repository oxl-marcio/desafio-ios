//
//  RepositoriesViewController.swift
//  desafio-ios
//
//  Created by Marcio Garcia on 1/20/17.
//  Copyright © 2017 Marcio Garcia. All rights reserved.
//

import UIKit

class RepositoriesViewController: UIViewController, LanguageDelegate {

    @IBOutlet weak var repoTableView: UITableView!
    
    var repoTableCellHeight: CGFloat = 0
    
    var requestResult: RequestResult?
    var itemList = [Repository]()
    
    var currentPage: Int = 1
    
    var selectedRepository: Repository?
    
    var selectedLanguage: Language?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tableCellNib = NSBundle.mainBundle().loadNibNamed(XIB_REPO_TABLE_CELL, owner: self, options: nil)
        let firstView = tableCellNib.first as? UIView
        self.repoTableCellHeight = (firstView?.bounds.size.height)!
        
        self.repoTableView.registerNib(UINib(nibName: XIB_REPO_TABLE_CELL, bundle: nil), forCellReuseIdentifier: ID_REPO_TABLE_CELL)
        
        self.repoTableView.dataSource = self
        self.repoTableView.delegate = self
        
        let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        activityIndicator.activityIndicatorViewStyle = .Gray
        activityIndicator.startAnimating()
        self.repoTableView.backgroundView = activityIndicator
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        
        self.selectedLanguage = Language(JSONString: "{\"id\":\"java\",\"name\":\"Java\"}")
        
        self.getNextResultPage()

    }
    
    func getNextResultPage()
    {
        if self.itemList.count < (self.requestResult?.totalCount ?? 0) {
            self.currentPage += 1
        }
        
        var searchCriteria = "language:"
        if let language = self.selectedLanguage {
            searchCriteria += language.id
        }
        
        GitHubService.getRepositories(searchCriteria, sort: SortType.Stars, page: self.currentPage) { (result) in
            
            if let result = result {
                self.requestResult = RequestResult(JSONString: result)
                if let items = self.requestResult?.items {
                    self.getRepositoriesFromResult(items)
                }
            }
            else {
                self.requestResult = nil
            }
            self.repoTableView.backgroundView = nil
            self.repoTableView.tableFooterView = nil
        }

    }
    
    func getRepositoriesFromResult(items: [Repository]) {
        for item in items {
            self.itemList.append(item)
        }
        self.repoTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Language Delegate
    
    func didSelectLanguage(language: Language) {
        
        self.selectedLanguage = language
        self.requestResult = nil
        self.itemList.removeAll()
        self.currentPage = 1
        self.repoTableView.setContentOffset(CGPointZero, animated: false)
        self.getNextResultPage()
        
    }
    
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == SEGUE_PULL_REQUEST {
            let destViewController = segue.destinationViewController as! PullRequestViewController
            destViewController.repositoryName = self.selectedRepository?.fullName
            destViewController.pullsUrl = self.selectedRepository?.pullsUrlFinal
        }
        else if segue.identifier == SEGUE_SELECT_LANGUAGE {
            let destViewController = segue.destinationViewController as! LanguageViewController
            destViewController.selectedLanguage = self.selectedLanguage
            destViewController.delegate = self
        }
    }
    

}

// MARK: - TableView

extension RepositoriesViewController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if self.itemList.count > 0 {
            if let cell = self.repoTableView.dequeueReusableCellWithIdentifier(ID_REPO_TABLE_CELL, forIndexPath: indexPath) as? RepositoryTableViewCell {
                
                cell.configure(self.itemList[indexPath.row])
                
                return cell
            }
            else {
                return UITableViewCell()
            }
        }
        else {
            return UITableViewCell()
        }

    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self.repoTableCellHeight
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.row == (self.itemList.count - 1) {
            
            let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
            activityIndicator.activityIndicatorViewStyle = .Gray
            activityIndicator.startAnimating()
            self.repoTableView.tableFooterView = activityIndicator
            
            if itemList.count < (requestResult?.totalCount ?? 0) {
                self.getNextResultPage()
            }
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        self.selectedRepository = self.itemList[indexPath.item]
        self.performSegueWithIdentifier(SEGUE_PULL_REQUEST, sender: self)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
}

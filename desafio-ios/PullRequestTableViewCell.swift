//
//  PullRequestTableViewCell.swift
//  desafio-ios
//
//  Created by Marcio Garcia on 1/22/17.
//  Copyright © 2017 Marcio Garcia. All rights reserved.
//

import UIKit

class PullRequestTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var userProfileImageView: UIImageView!
    @IBOutlet weak var userLoginLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userImageActivityIndicatorView: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.containerView.layer.cornerRadius = 4.0
        self.containerView.layer.borderWidth = 1
        self.containerView.layer.borderColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.1).CGColor
    }

    override func drawRect(rect: CGRect) {
        self.userProfileImageView.layer.cornerRadius = 5.0
        self.userProfileImageView.clipsToBounds = true
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(pullRequest: PullRequest) {
        
        if let title = pullRequest.title {
            self.titleLabel.text = title
        }
        else {
            self.titleLabel.text = "No Title"
        }
        
        if let description = pullRequest.body {
            self.descriptionLabel.text = description
        }
        
        if  let user = pullRequest.user {
            
            self.userLoginLabel.text = user.login
            
            if user.name?.characters.count > 0 {
                self.userNameLabel.text = user.name
            }
            else {
                if let url = user.url {
                    GitHubService.getUser(url, completion: { (result) in
                        if let result = result {
                            let mappedUser = Owner(JSONString: result)
                            user.name = mappedUser?.name
                            self.userNameLabel.text = user.name
                        }
                    })
                }
            }
            
            user.cancelImageRequest()
            self.userProfileImageView.image = nil
            self.userImageActivityIndicatorView.startAnimating()
            
            user.getProfileImage({ (image) in
                if let image = image {
                    self.userProfileImageView.image = image
                    self.userImageActivityIndicatorView.stopAnimating()
                }
            })
            
        }
    }


}

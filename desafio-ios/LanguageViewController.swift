//
//  LanguageViewController.swift
//  desafio-ios
//
//  Created by Marcio Garcia on 1/23/17.
//  Copyright © 2017 Marcio Garcia. All rights reserved.
//

import UIKit

class LanguageViewController: UIViewController {

    @IBOutlet weak var languagePickerView: UIPickerView!
    
    var pickerData = [Language]()
    
    var selectedLanguage: Language?
    var initialRow: Int = 0
    
     var delegate: LanguageDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.languagePickerView.dataSource = self
        self.languagePickerView.delegate = self
        
        if let JSON = Helper.loadJSON(FILE_LANGUAGES) {
            
            if let languages = JSON["languages"] as? [[String: AnyObject]] {
                var counter: Int = 0
                for lang in languages {
                    if let item = Language(JSON: lang) {
                        self.pickerData.append(item)
                        if item.id == self.selectedLanguage!.id {
                            self.initialRow = counter
                        }
                    }
                    counter += 1
                }
                self.languagePickerView.selectRow(self.initialRow, inComponent: 0, animated: true)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension LanguageViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickerData.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.pickerData[row].name
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        self.selectedLanguage = self.pickerData[row]
        
        if let language = self.selectedLanguage {
            print(language.id)
            self.delegate?.didSelectLanguage(language)
        }
    }
    
}

protocol LanguageDelegate {
    
    func didSelectLanguage(language: Language)
}

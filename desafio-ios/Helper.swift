//
//  Helper.swift
//  desafio-ios
//
//  Created by Marcio Garcia on 1/23/17.
//  Copyright © 2017 Marcio Garcia. All rights reserved.
//

import Foundation

class Helper {
    
    static func loadJSON(fileName: String) -> [String: AnyObject]? {
        
        let bundle = NSBundle.mainBundle()
        guard let pathString = bundle.pathForResource(fileName, ofType: "json") else {
            fatalError(fileName + " not found")
        }
        
        guard let jsonString = try? NSString(contentsOfFile: pathString, encoding: NSUTF8StringEncoding) else {
            fatalError("Unable to convert " + fileName + ".json to String")
        }
        
        guard let jsonData = jsonString.dataUsingEncoding(NSUTF8StringEncoding) else {
            fatalError("Unable to convert " + fileName + ".json to NSData")
        }
        
        guard let jsonDictionary = try? NSJSONSerialization.JSONObjectWithData(jsonData, options: []) as? [String:AnyObject] else {
            fatalError("Unable to convert " + fileName + " to JSON dictionary")
        }
        
        return jsonDictionary
    }
}

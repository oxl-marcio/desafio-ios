//
//  PullRequestViewController.swift
//  desafio-ios
//
//  Created by Marcio Garcia on 1/22/17.
//  Copyright © 2017 Marcio Garcia. All rights reserved.
//

import UIKit

class PullRequestViewController: UIViewController {

    @IBOutlet weak var pullRequestTableView: UITableView!
    
    var repositoryName: String?
    var pullsUrl: String?
    
    var tableCellHeight: CGFloat = 0

    var itemList = [PullRequest]()
    var totalOpen = 0
    var totalClosed = 0
    
    var selectedPullRequest: PullRequest?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = self.repositoryName
        
        let tableCellNib = NSBundle.mainBundle().loadNibNamed(XIB_PULL_REQUEST_TABLE_CELL, owner: self, options: nil)
        let firstView = tableCellNib.first as? UIView
        self.tableCellHeight = (firstView?.bounds.size.height)!
        
        self.pullRequestTableView.registerNib(UINib(nibName: XIB_PULL_REQUEST_TABLE_CELL, bundle: nil), forCellReuseIdentifier: ID_PULL_REQUEST_TABLE_CELL)
        
        self.pullRequestTableView.dataSource = self
        self.pullRequestTableView.delegate = self
        
        let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: -100, y: 0, width: 100, height: 100))
        activityIndicator.activityIndicatorViewStyle = .Gray
        activityIndicator.startAnimating()
        self.pullRequestTableView.backgroundView = activityIndicator
        
        //self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        
        self.getPullRequests()

    }

    func getPullRequests()
    {
        if let url = self.pullsUrl {
            GitHubService.getPullRequests(url) { (result, error) in
                
                if let error = error {
                    print(error.localizedDescription)
                    print(error.userInfo)
                    let label = self.createMessageLabel(CGRect(x: 0, y: 0, width: 300, height: 100), text: error.userInfo.description, fontSize: 17.0)
                    self.pullRequestTableView.backgroundView = label
                }
                else if let result = result {
                    for item in result {
                        if let pullRequest = PullRequest(JSON: item) {
                            self.itemList.append(pullRequest)
                            if pullRequest.state == "open" {
                                self.totalOpen += 1
                            }
                            else {
                                self.totalClosed += 1
                            }
                        }
                    }
                    let label = self.createMessageLabel(CGRect(x: 0, y: 0, width: 300, height: 20), text: "open \(self.totalOpen) / closed \(self.totalClosed)", fontSize: 12.0)
                    self.pullRequestTableView.tableHeaderView = label
                    self.pullRequestTableView.reloadData()
                    self.pullRequestTableView.backgroundView = nil
                }
            }
        }
    }

    func createMessageLabel(frame: CGRect, text: String, fontSize: CGFloat) -> UILabel {
        
        let title: UILabel = UILabel(frame: frame)
        
        title.textAlignment = NSTextAlignment.Center
        title.numberOfLines = 0
        title.text = text
        title.textColor = UIColor.darkGrayColor()
        title.font = UIFont.boldSystemFontOfSize(fontSize)
        
        let constraint = NSLayoutConstraint.constraintsWithVisualFormat("H:[label]", options: NSLayoutFormatOptions.AlignAllCenterX, metrics: nil, views: ["label": title])
        
        title.addConstraints(constraint)
        
        return title
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

}

// MARK: - TableView

extension PullRequestViewController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if let cell = self.pullRequestTableView.dequeueReusableCellWithIdentifier(ID_PULL_REQUEST_TABLE_CELL, forIndexPath: indexPath) as? PullRequestTableViewCell {
            
            cell.configure(self.itemList[indexPath.row])
            
            return cell
        }
        else {
            return UITableViewCell()
        }
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self.tableCellHeight
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        self.selectedPullRequest = self.itemList[indexPath.item]
        
        if let htmlUrl = self.selectedPullRequest?.htmlUrl {
            if let url = NSURL(string: htmlUrl) {
                UIApplication.sharedApplication().openURL(url)
            }
        }

        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }

}


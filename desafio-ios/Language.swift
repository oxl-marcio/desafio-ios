//
//  Language.swift
//  desafio-ios
//
//  Created by Marcio Garcia on 1/23/17.
//  Copyright © 2017 Marcio Garcia. All rights reserved.
//

import Foundation
import ObjectMapper

class Language: Mappable {
    
    var id: String!
    var name: String!
    
    required init?(_ map: Map){}
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
    }
}
//
//  Owner.swift
//  desafio-ios
//
//  Created by Marcio Garcia on 1/21/17.
//  Copyright © 2017 Marcio Garcia. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire

class Owner: Mappable {
    
    var id: Int?
    var login: String?
    var name: String?
    var profileImageUrl: String?
    var url: String?

    private var profileImageRequest: Request?
    
    required init?(_ map: Map){}

    func mapping(map: Map) {
        id <- map["id"]
        login <- map["login"]
        name <- map["name"]
        profileImageUrl <- map["avatar_url"]
        url <- map["url"]
    }
    
    func getProfileImage(completion: (UIImage?) -> Void) {
        
        if let url = self.profileImageUrl {
            
            self.profileImageRequest = ImageService.getImageByUrl(url) { (data) in
                
                if let data = data {
                    let image = UIImage(data: data)
                    completion(image)
                }
                else {
                    completion(nil)
                }
            }
        }
    }
    
    func cancelImageRequest() {
        if let request = self.profileImageRequest {
            request.cancel()
        }
    }
}
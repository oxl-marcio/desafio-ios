//
//  ImageService.swift
//  desafio-ios
//
//  Created by Marcio Garcia on 1/21/17.
//  Copyright © 2017 Marcio Garcia. All rights reserved.
//

import Foundation
import Alamofire

class ImageService {
    
    static private var _imageCache = NSCache()
    
    static func setImageCacheCostLimit(costInBytes: Int) {
        self._imageCache.totalCostLimit = costInBytes
    }
    
    static func getImageByUrl(url: String, completion: (NSData?) -> Void) -> Request? {
        
        var request: Request? = nil
        
        if let data = self._imageCache.objectForKey(url) as? NSData {
            print("GET IMAGE FROM CACHE")
            completion(data)
        }
        else {
            print("DOWNLOAD IMAGE")
            request = Alamofire.request(.GET, url).validate(contentType: ["image/*"]).responseData { (response) in
                
                if let data = response.result.value {
                    print("SAVE IMAGE INTO CACHE")
                    self._imageCache.setObject(data, forKey: url, cost: data.length)
                    completion(data)
                }
                else {
                    completion(nil)
                }
            }
        }
        
        return request
    }
    
}
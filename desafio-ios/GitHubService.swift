//
//  GitHubService.swift
//  desafio-ios
//
//  Created by Marcio Garcia on 1/21/17.
//  Copyright © 2017 Marcio Garcia. All rights reserved.
//

import Foundation
import Alamofire

enum SortType: String {
    case Stars = "stars"
    case Forks = "forks"
    case Updated = "updated"
}

class GitHubService {
    
    static func getRepositories(keyword: String, sort: SortType, page: Int, completion: (result: String?) -> Void ) {
        
        let url = "https://api.github.com/search/repositories?q=\(keyword)&sort=\(sort)&page=\(page)"
        
        Alamofire.request(.GET, url).validate(contentType: ["application/json"]).responseString(completionHandler: { (response) in
            
            if let result = response.result.value {
                completion(result: result)
            }
            else {
                completion(result: nil)
            }
        })
    }
    
    static func getUser(url: String, completion: (result: String?) -> Void ) {
        
        Alamofire.request(.GET, url).validate(contentType: ["application/json"]).responseString(completionHandler: { (response) in
            
            if let result = response.result.value {
                completion(result: result)
            }
            else {
                completion(result: nil)
            }
        })
        
    }
    
    static func getPullRequests(url: String, completion: (result: [[String: AnyObject]]?, error: NSError?) -> Void) {
        
        Alamofire.request(.GET, url).validate(contentType: ["application/json"]).responseJSON { (response) in
            
            switch response.result {
                case .Success(let JSON):
                    
                    if let resultAsArray = JSON as? NSArray {
                        let result = resultAsArray as? [[String : AnyObject]]
                        completion(result: result, error: nil)
                    }
                    else if let resultAsDictionary = JSON as? NSDictionary {
                        if let message = resultAsDictionary.objectForKey("message") {
                            var userInfo = [NSObject : AnyObject]()
                            userInfo["message"] = message
                            let error = NSError(domain: "GitHubService", code: 100, userInfo: userInfo)
                            completion(result: nil, error: error)
                        }
                    } else {
                        var userInfo = [NSObject : AnyObject]()
                        userInfo["message"] = "Unknown Error"
                        let error = NSError(domain: "GitHubService", code: 100, userInfo: userInfo)
                        completion(result: nil, error: error)
                    }
                
                case .Failure(let error):
                    completion(result: nil, error: error)
            }
        }
    }
}
